﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {


	public float maxSpeed = 5.0f; // in metres per second
	public float acceleration = 1.0f; // in metres/second/second
    public float brake = 5.0f;
    public float turnSpeed = 30.0f; // in degrees/second
	private float speed = 0.0f;    // in metres/second

	void Update() {
		

		// DRIVE, the vertical axis controls acceleration fwd/back
		float forwards = Input.GetAxis("Vertical");
		if (forwards > 0) {
			// forwards
			speed = speed + acceleration * Time.deltaTime;
		}

		// backwards
		else if (forwards < 0) {
			speed = speed - acceleration * Time.deltaTime;



		} else {
			// braking
			if (speed > 0) {
				speed = speed - brake * Time.deltaTime;
			} else {

				if (speed < 0) {
					speed = speed + brake * Time.deltaTime;
			
				}
			}
		}



         // clamp the speed
         speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);



		// compute a vector in the up direction of length speed
		Vector2 velocity = Vector2.up * speed;

		// move the object
		transform.Translate(velocity * Time.deltaTime, Space.Self);

		// ROTATE, the horizontal axis controls the turn
		float turn = Input.GetAxis("Horizontal");
		transform.Rotate(0, 0, turn * speed * -turnSpeed * Time.deltaTime);

	}
}
